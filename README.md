# Zephyr RPMSG client driver #

## Build instructions ##

In order to compile and install this driver, simply run the following commands:

```
make -C /lib/modules/`uname -r`/build M=$PWD
sudo make -C /lib/modules/`uname -r`/build M=$PWD modules_install
```
